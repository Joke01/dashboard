<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                font-size: 25px;
            }

            .title {
                font-size: 84px;
            }

            .subtitle {
                font-size: 44px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px; 
                letter-spacing: .1rem;
            }

            .myButton {
                border-style: solid;
                border-color: #636b6f;
                border-width: 3px;
                background-color:  #f2f2f2;
                text-align: left;
                padding: 10px;
                font-size: 25px;   
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                
                    <?php
                    session_start();
                    $tokenExists = array_key_exists('userToken', $_SESSION);
                    if ($tokenExists) {
                        $user_token = $_SESSION['userToken'];
                    
                        $headers = [
                            'Authorization' => 'Bearer ' . $user_token,        
                            'Accept'        => 'application/json',
                        ];

                        $client = new GuzzleHttp\Client(['base_uri' => 'https://api-ssl.bitly.com/v4/']);
                 
                        $response = $client->request('GET','user',[
                            'headers' => $headers,
                            'http_errors' => false,
                        ]);
                        $status =  $response->getStatusCode();
                        if ($status == "200") {

                            $contents = $response->getBody()->getContents();
                            $data = json_decode($contents, true);
                            $name = $data['name'];
                            $group = $data['default_group_guid'];
                            $_SESSION['group'] = $group;
                            echo ' <div class="title m-b-md"> Welcome, '
                            .$name.'</div>';
                            $client = new GuzzleHttp\Client(['base_uri' => 'https://api-ssl.bitly.com/v4/']);
                            $response = $client->request('GET','groups',[
                                'headers' => $headers,
                                'http_errors' => false,
                            ]);
                            $contents = $response->getBody()->getContents();
                            echo ' <div class="subtitle m-b-md"> Your Bitly Groups: </div>';
                            $data = json_decode($contents, true);
                            $groups = $data['groups'];
                            $arrayLength = count($groups);
                            $i = 0;
                            while ($i < $arrayLength) { 
                        
                                $j = $i + 1;
                                echo '<div class="links">group: '.$j." ";
                                $id = $groups[$i];
                                $guid = $id['guid'];
                                echo $guid;
                                $url = route('groupurls')."/".$guid;
                                echo '<a href="'.$url.'">View url collection </a>'."<br /></div>";
                                $i++;
                            }

                        } else {
                        
                            echo "something went wrong";
                            $url = route('welcome');
                            $urlButton = '<br><a href="'.$url.'"> <button class="myButton">Back to login page</button> </a>';
                            echo $urlButton;
                        }

                    } else {
                        
                        echo "something went wrong";
                        $url = route('welcome');
                        $urlButton = '<br><a href="'.$url.'"> <button class="myButton">Back to login page</button> </a>';
                        echo $urlButton;

                    }
                    
                    ?>

            
              
            </div>
        </div>
    </body>
</html>