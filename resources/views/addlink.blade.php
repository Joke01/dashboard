<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                
                text-align: center;
                font-size: 25px;
            }

            .title {
                font-size: 84px;
            }

            .subtitle {
                font-size: 44px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px; 
                letter-spacing: .1rem;
                text-align: left;
               
            }
            .links   {
                padding: 10px;
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .myButton {
                border-style: solid;
                border-color: #636b6f;
                border-width: 3px;
                background-color:  #f2f2f2;
                text-align: left;
                padding: 10px;
                font-size: 25px;   
            }

            .text{
                width: 400px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="subtitle m-b-md">
                    Create a new Bitly Url
                </div>
                <div class="links">
                    <form action="submitnew" method="get">
                        <label for="longUrl">Original url</label> <br>
                        <input type="url" name="longUrl" class="text" required><br>
                        Title of your new Bitly Url: 
                        <br><input type="text" name="title"class="text" required><br>
                        <button type="submit"class="myButton">Save link!</button>
                    </form>

                </div>
                <div class="links">
                <?php 
                    session_start();
                    $groupId = $_SESSION['groupId'];
                    $url = route('groupurls')."/".$groupId;
                    $myButton = '<a href="'.$url.'"> <button class="myButton">Back to url collection</button> </a>';
                    echo $myButton;
                    ?>
                </div>
            </div>
        </div>
    </body>
</html>