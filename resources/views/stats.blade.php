<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .subtitle {
                font-size: 44px;
            }

            .urls {
                border-style: solid;
                background-color:  #f2f2f2;
                text-align: left;
                padding-left: 30px;  
                font-size: 25px;
                padding-right:10px;
            }


            .myButton {
                border-style: solid;
                border-color: #636b6f;
                border-width: 3px;
                background-color:  #f2f2f2;
                text-align: left;
                padding: 10px;
                font-size: 25px;   
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <?php
                echo'<div class="subtitle m-b-md">Statistics for url id: '.$stats.'</div>';
                session_start();
                $tokenExists = array_key_exists('userToken', $_SESSION);
                $guidExists = array_key_exists('groupId', $_SESSION);
                if ($tokenExists && $guidExists) {
                    
                    $user_token = $_SESSION['userToken'];
                    $headers = [
                        'Authorization' => 'Bearer ' . $user_token,        
                        'Accept'        => 'application/json',
                    ];
                    $client = new GuzzleHttp\Client(['base_uri' => 'https://api-ssl.bitly.com/v4/bitlinks/']);
                    $response = $client->request('GET', $stats.'/clicks/summary', [
                         'headers' => $headers
                    ]);
                    $contents = $response->getBody()->getContents();
                    $data = json_decode($contents, true);
                    $totalNumberOfClicks = $data['total_clicks'];
                    echo'<div class="subtitle m-b-md">Total number of clicks: '.$totalNumberOfClicks.'</div>';
                    $client = new GuzzleHttp\Client(['base_uri' => 'https://api-ssl.bitly.com/v4/bitlinks/']);
                    
                    $response = $client->request('GET', $stats.'/referrers',[
                         'headers' => $headers
                    ]);

                    $contents = $response->getBody()->getContents();
                    $data = json_decode($contents, true);
                    $metrics = $data['metrics'];
                    $arrayLength = count($metrics);
                   
                        if ($arrayLength <= 0) {

                            echo'<div class="subtitle m-b-md">no referrals for this link</div>';

                        } else {

                        echo'<div class="subtitle m-b-md">Referrals for this link:</div>';
                        $i = 0;
                        while ($i < $arrayLength)  { 
                            
                            $linkObj = $metrics[$i];
                            $referrer = $linkObj['value'];
                            $clicks = $linkObj['clicks'];
                            $metricContents = '<div class="urls"> Referrer: '
                            .$referrer.'<br> Clicks: '.$clicks.'</div>';
                            echo $metricContents;
                            echo "<br>";
                            $i++;
                        }
                        $response = $client->request('GET', $stats.'/countries',[
                            'headers' => $headers
                        ]);
                        $contents = $response->getBody()->getContents();
                        $data = json_decode($contents, true);
                        echo'<div class="subtitle m-b-md">Clicks per country :</div>';
                        $stats = $data['metrics'];
                        $arrayLength = count($stats);
                        $i = 0;
                        while ($i < $arrayLength) {  
                                
                                $statObj = $stats[$i];
                                $country = $statObj['value'];
                                $clicks = $statObj['clicks'];
                                $statContents = '<div class="urls"> Country: '
                                .$country.'<br> Clicks: '.$clicks.'</div>';
                                echo $statContents;
                                $i++;

                            }
                        }
                    $groupId = $_SESSION['groupId'];
                    $url = route('groupurls')."/".$groupId;
                    $myButton = '<a href="'.$url.'"> <button class="myButton">Back to url collection</button> </a>';
                    echo $myButton;

                } else {
                    echo "something went wrong";
                    $url = route('welcome');
                    $urlButton = '<br><a href="'.$url.'"> <button class="myButton">Back to login page</button> </a>';
                    echo $urlButton;
                }
  
                    ?>  
                    <div class="links">
                       
                    </div>
                </div>
        </div>
    </body>
</html>
