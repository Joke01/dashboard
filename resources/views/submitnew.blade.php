<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                
                text-align: center;
                font-size: 25px;
            }

            .title {
                font-size: 84px;
            }

            .subtitle {
                font-size: 44px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px; 
                letter-spacing: .1rem;
                text-align: left;
               
            }
            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .urls {
                border-style: solid;
                background-color:  #f2f2f2;
                text-align: left;
                padding-left: 30px;
                padding-right: 30px;
               
            }
            .myButton {
                border-style: solid;
                border-color: #636b6f;
                border-width: 3px;
                background-color:  #f2f2f2;
                text-align: left;
                padding: 10px;
                font-size: 25px;   
            }

            .text{
                width: 400px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="links">
                    <div class="subtitle m-b-md">
                        Submitted
                    </div>
                    <?php 
                    session_start();
                    $groupId = $_SESSION['groupId'];
                    $user_token = $_SESSION['userToken'];
                    
                    $headers = [
                        'Authorization' => 'Bearer ' . $user_token,        
                        'Accept'        => 'application/json',
                    ];

                    $client = new GuzzleHttp\Client(['base_uri' => 'https://api-ssl.bitly.com/v4/']);
               
                    $bodyArray = [
                        "domain"=> "bit.ly",
                        "group_guid"=>  $groupId,
                        "title" => $_GET["title"],
                        "long_url" => $_GET["longUrl"]
                    ];
                    $bodyString = json_encode($bodyArray);

                    $response = $client->request('POST','bitlinks',[
                        'headers' => $headers,
                        'body'=> $bodyString,
                    ]);

                    $contents = $response->getBody()->getContents();
                    $data = json_decode($contents, true);
                    $decodedContents = json_decode($contents, true);
                    $longUrl = $decodedContents['long_url'];
                    $shortUrl = $decodedContents['link'];
                    $title = $decodedContents['title'];
                    $linkContents = '<div class="urls">Your new Bitly url! <br> Original Link: '
                    .$longUrl."<br /> Bitly Link: ".$shortUrl.'<br>Title: '.$title.' </div> <br />';
                    echo $linkContents;
                    $url = route('overview');
                    ?> 
                    <div class="links">
                        <?php
                        $url = route('groupurls')."/".$groupId;
                        $myButton = '<a href="'.$url.'"> <button class="myButton">Back to url collection</button> </a>';
                        echo $myButton;
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>