<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .myButton {
                border-style: solid;
                border-color: #636b6f;
                border-width: 3px;
                background-color:  #f2f2f2;
                text-align: left;
                padding: 10px;
                font-size: 25px;   
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
        
                <div class="links">
                <?php
                    use Illuminate\Support\Str;
                    use GuzzleHttp\Client;
                    session_start();
                    $tokenExists = array_key_exists('userToken', $_SESSION);
                    if (!$tokenExists) {
                        $client = new Client();
                        $headers = [     
                            'Content-type'  => 'application/x-www-form-urlencoded'
                        ];
                        $response = $client->request('POST','https://api-ssl.bitly.com/oauth/access_token',[
                            'headers' => $headers,
                            'http_errors' => false,
                            'form_params' => [ 
                            'client_id' => '17c4178dafbf68c4c6523dd7c2d8a2986c4205db',
                            'client_secret'=>'2a324f8a34010f2d462e15b9634efd3dab98ec61',
                            'code'=> $code,
                            'redirect_uri'=>'https://fortesting.nl/landingpage/',
                            
                            ]
                        ]);
                        $status =  $response->getStatusCode();
                        if ($status == "200") {
                            $contents = $response->getBody()->getContents();
                            $userToken = Str::between($contents, '=', '&');
                            $_SESSION['userToken'] = $userToken;
                            echo '<div class="title m-b-md">
                            Landing page after rerouting
                            </div>';
                            $url = route('overview');
                            $urlButton = '<br><a href="'.$url.'"> <button class="myButton">Continue to personal page</button> </a>';
                            echo $urlButton;
                            
                        } else 
                        {
                            echo "something went wrong";
                            $url = route('welcome');
                            $urlButton = '<br><a href="'.$url.'"> <button class="myButton">Back to login page</button> </a>';
                            echo $urlButton;
                        }
                     
                    } else {
                        echo '<div class="title m-b-md">
                        Landing page after rerouting
                        </div>';
                        $url = route('overview');
                        $urlButton = '<br><a href="'.$url.'"> <button class="myButton">Continue to personal page</button> </a>';
                        echo $urlButton;

                    }
                    ?>

                </div>
                <div class="links">
        

                </div>
                </div>
            </div>
        </div>
    </body>
</html>