<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links   {
                padding: 10px;
            }

            .subtitle {
                font-size: 44px;
            }

            .urls {
                border-style: solid;
                background-color:  #f2f2f2;
                text-align: left;
                padding-left: 30px;  
                font-size: 25px;
                padding-right:10px;
            }


            .myButton {
                border-style: solid;
                border-color: #636b6f;
                border-width: 3px;
                background-color:  #f2f2f2;
                text-align: left;
                padding: 10px;
                font-size: 25px;   
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="subtitle m-b-md">
                    Edit title
                </div>
                <div class="links">
                    <?php
                    use Illuminate\Support\Str;
                    $title = Str::of($edit)->after('/?title=');
                    $id = Str::beforeLast($edit, '/?title=');
                    ?>
                    <form action="submitedit" method="get">
                        <input type="text" id="title" name="title" value="{{$title}}"><br>
                        <input type="text" id="id" name="id" value="{{$id}}" hidden><br>
                        <input type="submit" class="myButton" value="Submit">
                    </form>
                </div>
                <div class="links">
                    <?php 
                    session_start();
                    $groupId = $_SESSION['groupId'];
                    $url = route('groupurls')."/".$groupId;
                    $myButton = '<a href="'.$url.'"> <button class="myButton">Back to url collection</button> </a>';
                    echo $myButton;
                    ?>
                </div>
        </div>
    </body>
</html>


