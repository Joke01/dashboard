<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');


Route::get('/landingpage', function () {
    $keyExists = array_key_exists('code', $_GET);
    if ($keyExists) 
    {
        $code = $_GET['code'];
        return view('landingpage',['code'=> $code]);
    } else {
        return view('welcome');
    }
})->name('landingpage');



Route::get('/overview', function () {
    return view('overview');
})->name('overview');


Route::get('/groupurls/{groupId?}', function ($groupId) {
    return view('groupurls', ['guid' => $groupId]);
})->name('groupurls');


Route::get('/stats', function () {
    $stats = $_GET['stats'];
    return view('stats',['stats' => $stats]);
})->name('stats');


Route::get('/edit', function () {
    $edit = $_GET['edit'];
    return view('edit',['edit' => $edit]);
})->name('edit');


Route::get('/submitedit', function () {
    return view('submitedit');
});

Route::get('/addlink', function () {
    return view('addlink');
})->name('addlink');


Route::get('/submitnew', function () {
    return view('submitnew');
});


