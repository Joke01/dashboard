<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>


## Update (14-06-2020)

Het dashboard staat inmiddels online. Het is nu in staat om Bitly users te authenticeren. Het is te vinden op https://fortesting.nl/ 

**Mijn dashboard**
- Biedt gebruikers de mogelijkheid om in te loggen met hun Bitly account 
- Toont informatie van het Bitly account van gebruiker
- Toont informatie over de groepen van de gebruiker
- Toont links per groep
- Biedt de mogelijkheid om nieuwe links aan te maken
- Biedt de mogelijkheid om de titel van een link aan te passen  
- Biedt de mogelijkheid om (enkele) statistieken van een link in te zien 


## Uitleg Dashboard app (11-06-2020)
 
**Mijn dashboard tot nu toe**
- Toont informatie van gebruiker
- Toont informatie over de groepen van de gebruiker
- Toont links per groep
- Biedt de mogelijkheid om nieuwe links aan te maken
 
**Nog te doen**

- Optie om links te bewerken
- Optie om links te verbergen
- Optie om statistieken van links in te zien
- Volledig werkende login procedure
 
De login procedure nu 
Het grootste deel van de authenticatie procedure is al af. Mijn dashboard is in staat om met een Bitly code een security token op te halen en op te slaan. Vervolgens kan hij daar calls naar de Bitly api mee te doen. 
 
Wat nog niet af is: het verkrijgen van die code. 
Die code wordt verkregen door de gebruikers van jouw applicatie door te sturen naar de Bitly inlog pagina. Dat doe je met een speciale link. In die link geef oa je een bepaalde pagina van je applicatie op. Na het inloggen worden de users door Bitly doorgestuurd naar die pagina, met de benodigde code in de url. Die pagina vangt die code vervolgens op en gebruikt die om een security token op te halen. 

Ik heb mijn app nu nog lokaal draaien. Heb nog niet eerder met PhP/Laravel gewerkt, dus ben niet bekend met de procedure om apps te deployen). Ik weet (nog) niet of en hoe die app te bereiken is van buitenaf. Daarom is hij nog niet in staat die redirect van Bitly op te vangen. 
 
Daarom moet je dat stuk nog ‘met de hand’ doen. Dat gaat als volgt: In de welcome view van het dashboard staat een knop. Onder die knop de link waarmee je als user doorgestuurd wordt naar Bitly.
 
In die link is als return pagina een simpele php pagina opgegeven. Die pagina staat al wel online: http://fortesting.nl/. Na het klikken en inloggen kom je dus op die pagina. Daar pluk je de code uit de adresbalk. Vervolgens ga je met die code naar de view landingpage. Van daar af kan je ‘gewoon’ met de site werken.
 
(de benodigde client_secret en client_id) vind je ook in de link in de welcome view. En onderaan deze paragraaf. 
 
Voor nu ga ik verder met uitzoeken hoe ik het dashboard online kan zetten, zodat ik de authenticatieprocedure compleet kan maken. 

## login
https://bitly.com/oauth/authorize?client_id=17c4178dafbf68c4c6523dd7c2d8a2986c4205db&redirect_uri=http://fortesting.nl/

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.

## Laravel Sponsors

We would like to extend our thanks to the following sponsors for funding Laravel development. If you are interested in becoming a sponsor, please visit the Laravel [Patreon page](https://patreon.com/taylorotwell).

### Premium Partners

- **[Vehikl](https://vehikl.com/)**
- **[Tighten Co.](https://tighten.co)**
- **[Kirschbaum Development Group](https://kirschbaumdevelopment.com)**
- **[64 Robots](https://64robots.com)**
- **[Cubet Techno Labs](https://cubettech.com)**
- **[Cyber-Duck](https://cyber-duck.co.uk)**
- **[Many](https://www.many.co.uk)**
- **[Webdock, Fast VPS Hosting](https://www.webdock.io/en)**
- **[DevSquad](https://devsquad.com)**

### Community Sponsors

<a href="https://op.gg"><img src="http://opgg-static.akamaized.net/icon/t.rectangle.png" width="150"></a>

- [UserInsights](https://userinsights.com)
- [Fragrantica](https://www.fragrantica.com)
- [SOFTonSOFA](https://softonsofa.com/)
- [User10](https://user10.com)
- [Soumettre.fr](https://soumettre.fr/)
- [CodeBrisk](https://codebrisk.com)
- [1Forge](https://1forge.com)
- [TECPRESSO](https://tecpresso.co.jp/)
- [Runtime Converter](http://runtimeconverter.com/)
- [WebL'Agence](https://weblagence.com/)
- [Invoice Ninja](https://www.invoiceninja.com)
- [iMi digital](https://www.imi-digital.de/)
- [Earthlink](https://www.earthlink.ro/)
- [Steadfast Collective](https://steadfastcollective.com/)
- [We Are The Robots Inc.](https://watr.mx/)
- [Understand.io](https://www.understand.io/)
- [Abdel Elrafa](https://abdelelrafa.com)
- [Hyper Host](https://hyper.host)
- [Appoly](https://www.appoly.co.uk)
- [云软科技](http://www.yunruan.ltd/)

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](https://laravel.com/docs/contributions).

## Code of Conduct

In order to ensure that the Laravel community is welcoming to all, please review and abide by the [Code of Conduct](https://laravel.com/docs/contributions#code-of-conduct).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell via [taylor@laravel.com](mailto:taylor@laravel.com). All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).

